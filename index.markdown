---
layout: default
title: Documentation for Development Environments exam app
---

# Development environments exam project

## Short overview
This project is a dockerized system including an Express.js backend with a React.js frontend. The project uses docker-compose and the repository includes a CI pipeline.

## How to install and run the project

1. Clone the project from this repository and navigate to the folder. 
2. Make sure the Docker Desptop app is running
3. Run the command *RTE=dev docker compose up* to start the live server.
4. Navigate to http://localhost:3050/ in your brower
5. You can test the connection between the apps working and data is saved to the database and the saved data is persisted after container restart.
6. Now you're all set!

## How to use the project
The applications are quite simple as the focus was on the infrasturcture side. The application saved and retrives the user input into a Postgres database
